# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(email: "abhishek.k@awign1.com", phone: "7763865781", name: "Abhishek Kumar1", verified: false, otp: 1)
User.create(email: "abhishek.k@awign2.com", phone: "7763865782", name: "Abhishek Kumar2", verified: false, otp: 1)
User.create(email: "abhishek.k@awign3.com", phone: "7763865783", name: "Abhishek Kumar3", verified: false, otp: 1)
User.create(email: "abhishek.k@awign4.com", phone: "7763865784", name: "Abhishek Kumar4", verified: false, otp: 1)