class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    drop_table :users
    create_table :users do |t|
      t.string :email
      t.string :phone
      t.string :string
      t.string :name
      t.string :string
      t.string :verified
      t.string :boolean
      t.integer :otp

      t.timestamps
    end
  end
end
