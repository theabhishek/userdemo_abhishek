class UserController < ApplicationController

    def index
        @users = User.all
        render json: @users
    end

    def show
        @user = User.find(params[:id])
        render json: @user

    end

    def create
        @user = User.new(user_params)
        @user.verified = false
        if @user.save
            redirect_to :action => 'index'
        end
    end

    def update
        @user = User.find(params[:id])
        if@user.update_attributes(user_params)
            @user.otp = generate_otp()
            @user.update_attributes(user_params)
            render json: {message: "Otp is : #{@user.otp} please verify"}
        end
    end

    def destroy
        User.find(params[:email]).destroy
        redirect_to :action => 'index'
    end
    

    def verify
        @user = User.find(params[:id])
        puts @user.otp
        puts params[:otp]
        if params[:otp] == @user.otp
          if @user.update_attribute(:verified,true)
            # puts "entered"
            render json: {message: "user verified successfuly"}
          end
        else
          render json: {message: "user verification failed"}
        end
    
    end
    
    private

    def generate_otp
        @user.otp = Random.rand(10000..99999)
    end

    def user_params
        params.permit(:email, :phone, :name)
    end


end
