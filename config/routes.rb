Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :user, :only => [:index, :show, :create, :update, :verify] do
    post 'verify', to: 'user#verify'
  end
  end